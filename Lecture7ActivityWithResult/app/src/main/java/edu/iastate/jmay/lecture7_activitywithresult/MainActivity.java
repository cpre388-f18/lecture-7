package edu.iastate.jmay.lecture7_activitywithresult;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    /** This is our MVC "model".  We can use a built in Java class instead of creating our own. */
    private String userName = "name";
    /** The TextView on the layout showing the user name. */
    private TextView nameTextView;
    /**
     * The unique request code for distinguishing the result in the onActivityResult() event
     * handler.
     */
    private static final int NAME_REQUEST_CODE = 1;
    /**
     * The key for the Intent sent from setResult() in NameActivity and received in
     * onActivityResult() in this class.
     */
    private static final String KEY_RETURN_NAME = "edu.iastate.jmay.lecture7_activitywithresult.return_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameTextView = findViewById(R.id.nameTextView);
        updateView();
    }

    public void onEditClick(View v) {
        // This is a standard explicit intent.
        Intent explicitIntent = NameActivity.createIntent(this, userName);
        // The explicit intent is launched with the request that the result will trigger an event to
        // onActivityResult() with the same request code.
        startActivityForResult(explicitIntent, NAME_REQUEST_CODE);
    }

    /**
     * This event handler is called whenever an activity started with startActivityForResult()
     * finishes.
     * @param requestCode the request code given to startActivityForResult() (e.g. line 30)
     * @param resultCode the result code set in NameActivity with setResult() (e.g. line 35 & 40).
     *                   Defaults to RESULT_CANCELED if the activity never called setResult().
     * @param data optional extra data that the other activity may return to this one via an Intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case NAME_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    userName = data.getStringExtra(KEY_RETURN_NAME);
//                    userName = data.getStringExtra("name");
                    updateView();
                }
                break;
        }
    }

    /**
     * Helper function for updating the layout with the model data.
     */
    private void updateView() {
        nameTextView.setText(getString(R.string.your_name_format, userName));
    }

    public static Intent createNameResponseIntent(Context packageContext, String name) {
        // Create a generic intent for use with setResult()
        Intent intent = new Intent();
        intent.putExtra(KEY_RETURN_NAME, name);
        return intent;
    }
}
