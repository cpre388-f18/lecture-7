package edu.iastate.jmay.lecture7_activitywithresult;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class NameActivity extends AppCompatActivity {
    /** The unique key for passing the user name via an explicit intent to this Activity. */
    private static final String KEY_NAME = "edu.iastate.jmay.lecture7_activitywithresult.name";
    /** The "textbox" where the user can change the name. */
    private EditText nameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);

        nameEditText = findViewById(R.id.nameEditText);
        // Get the explicit intent from MainActivity:33.
        Intent intent = getIntent();
        nameEditText.setText(intent.getStringExtra(KEY_NAME));
    }

    /**
     * This static function creates explicit intents for use by other Activities.
     * @param packageContext the calling context (usually Activity)
     * @param name the user name to pass to this Activity in the Intent
     * @return the explicit intent that can be passed to startActivity() or startActivityForResult()
     */
    public static Intent createIntent(Context packageContext, String name) {
        Intent explicitIntent = new Intent(packageContext, NameActivity.class);
        explicitIntent.putExtra(KEY_NAME, name);
        return explicitIntent;
    }

    public void onOkClick(View v) {
        // Create a generic intent to store the extra return data.
//        Intent resultIntent = new Intent();
//        resultIntent.putExtra("name", nameEditText.getText().toString());
        // The code above didn't follow best practices.  The revision is below.
        Intent resultIntent = MainActivity.createNameResponseIntent(this, nameEditText.getText().toString());
        // Set the result and put the extra data that is associated with RESULT_OK in this case.
        setResult(Activity.RESULT_OK, resultIntent);
        // Calling finish exits this Activity and returns to the previous one.  It's similar to the
        // user pushing the back button to exit this Activity.
        finish();
    }

    public void onCancelClick(View v) {
        // Set a result, but no extra data is added.
        setResult(Activity.RESULT_CANCELED);
        // Call finish() to exit this activity.
        finish();
    }
}
