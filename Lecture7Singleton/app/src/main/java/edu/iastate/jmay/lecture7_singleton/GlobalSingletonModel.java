package edu.iastate.jmay.lecture7_singleton;

import java.util.ArrayList;

/**
 * This class implements the singleton design pattern.  It will outlive the Activities.
 */
class GlobalSingletonModel {
    /** Some data we want to share globally. */
    private int counter = 0;

    /**
     * (Singleton Pattern) The one instance of this class that will ever exist.  That's why it's
     * static.
     */
    private static GlobalSingletonModel ourInstance = null;

    /**
     * (Singleton Pattern) This is how other to "instantiate" the singleton from elsewhere.
     * Note how this is using the "synchronized" keyword.  Without it, multiple thread could call
     * getInstance() at the same time, causing multiple instances to be created and other
     * unpredictable behaviour.
     */
    static synchronized GlobalSingletonModel getInstance() {
        if (ourInstance == null) {
            ourInstance = new GlobalSingletonModel();
        }
        return ourInstance;
    }

    /** (Singleton Pattern) Hide the constructor from other classes to enforce only one instance. */
    private GlobalSingletonModel() { }

    /**
     * A setter.  Notice the "synchronized" keyword.  This is necessary to prevent data
     * inconsistencies with threading.
     */
    public synchronized void incrementCount() {
        counter++;
    }

    /**
     * A getter.  Notice the "synchronized" keyword.  This is necessary to prevent data
     * inconsistencies with threading.
     * @return the current counter value
     */
    public synchronized int getCounter() {
        return counter;
    }
}
