package edu.iastate.jmay.lecture7_singleton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class Main2Activity extends AppCompatActivity {
    // Another reference to the singleton.  This variable points to the same instance as
    // MainActivity does.
    private GlobalSingletonModel singleton = null;
    /** A reference to the TextView that shows the counter. */
    private TextView textView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // Locate the singleton instance.
        singleton = GlobalSingletonModel.getInstance();
        // Locate the TextView for use in the button event handler.
        textView = findViewById(R.id.textView);
        // Update the layout to display the number.
        updateView();
    }

    public void onButtonClick(View v) {
        // Call a setter on the singleton object.  This modifies the global value that is available
        // from MainActivity.
        singleton.incrementCount();
        // Update the layout to show the new number.
        updateView();
    }

    /**
     * A helper function that updates the layout with the information stored in the model.
     */
    private void updateView() {
        // singleton.getCounter() gets the value from the singleton instance.
        textView.setText(String.format(Locale.getDefault(), "%d", singleton.getCounter()));
    }
}
