package edu.iastate.jmay.lecture7_singleton;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    // The singleton is usually easiest referenced as an instance variable for use anywhere in the class.
    private GlobalSingletonModel singleton = null;
    /** The TextView showing the count value from the singleton. */
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Here's where the singleton is loaded.
        singleton = GlobalSingletonModel.getInstance();
        textView = findViewById(R.id.textView);
        updateView();
    }

    public void onButtonClick(View v) {
        // Calling a setter on the singleton.
        singleton.incrementCount();
        updateView();
    }

    public void onButton2Click(View v) {
        // Notice that this explicit intent is NOT sending any data.  The counter value is not
        // passed to Main2Activity via the intent.
        Intent activity2Intent = new Intent(this, Main2Activity.class);
        startActivity(activity2Intent);
    }

    private void updateView() {
        // The getter on the singleton is called.
        textView.setText(String.format(Locale.getDefault(), "%d", singleton.getCounter()));
    }
}
